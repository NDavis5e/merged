package com.example.operator1.gui_app_proto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Login extends AppCompatActivity {

    public Register reg;
    public boolean emailHere;
    public boolean passHere;
    public String remName;
    public EditText enEmail;
    public EditText enPass;
    public TextView error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button Login_3 = findViewById(R.id.Register);
        Button Register = findViewById(R.id.Login);
        Button Home = findViewById(R.id.button3);
        reg = new Register();

        enEmail = (EditText) findViewById(R.id.Enter_user);
        enPass = (EditText) findViewById(R.id.Enter_pass);
        error = (TextView) findViewById(R.id.textView2);

        final String passEntered = enPass.getText().toString();

        Login_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenShop();
            }
        });

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenRegister();
            }
        });

        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenHome();
            }
        });

    }

    public void OpenHome(){
        Intent intent = new Intent(this, SPlash.class);
        startActivity(intent);
    }

    public void OpenRegister(){
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }

    public void OpenShop(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
