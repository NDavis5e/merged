package com.example.operator1.gui_app_proto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SPlash extends AppCompatActivity {


    Register reg = new Register();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Button Login = (Button) findViewById(R.id.Login);
        Button Register = (Button) findViewById(R.id.Register);
        Button Shop = (Button) findViewById(R.id.QuickShop);

        reg.addList("JohnDoe", "adsdsad", "aaaaa");
        reg.addList("JaneDoe", "shdssff", "bbbbb");

        System.out.println(reg.emaill.size());
        System.out.println(reg.userl.size());
        System.out.println(reg.passl.size());

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenLogin();
            }
        });

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenRegister();
            }
        });

        Shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenShop();
            }
        });
    }

    public void OpenLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void OpenRegister(){
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }

    public void OpenShop(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

