package com.example.operator1.gui_app_proto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.operator1.gui_app_proto.R;

public class MainActivityC extends AppCompatActivity {
    Button btnToCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainc);

        btnToCheck = (Button) findViewById(R.id.mainbutt);
        TextView temp_receipt = findViewById(R.id.ListReciept);

        for(int i = 0; i < 10; i++){
            temp_receipt.setText("Item : Item #" + i + "  Cost: insert cost here \n");
        }

        btnToCheck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                    //open activity
                    Intent intent = new Intent(MainActivityC.this, checkout.class);
                    startActivity(intent);

            }
        });

    }
}
