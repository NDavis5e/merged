package com.example.operator1.gui_app_proto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button Home = findViewById(R.id.Home);
        Button Checkout = findViewById(R.id.checkOut);
        Button listActi = findViewById(R.id.start_search);

        String[] items = {"chocolate"};
        ListAdapter adapter = new com.example.operator1.gui_app_proto.ImageAdapter(this, items);
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String item = String.valueOf(parent.getItemIdAtPosition(position));
                        Toast.makeText(MainActivity.this, item, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        Home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenHome();
            }
        });

        Checkout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goCheckout();
            }
        });
    }

    public void OpenHome(){
        Intent intent = new Intent(this, SPlash.class);
        startActivity(intent);
    }

    public void goCheckout(){
        Intent intent = new Intent(this, MainActivityC.class);
        startActivity(intent);
    }
    //added by Julien Kabagema
    //will be used to sync with the search activity created
    public void GoListAcitvity(){
        Intent intent = new Intent (this, ListSearchView.class);
        startActivity(intent);
    }
}