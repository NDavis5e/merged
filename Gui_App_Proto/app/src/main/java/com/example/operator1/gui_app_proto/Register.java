package com.example.operator1.gui_app_proto;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Register extends AppCompatActivity {

    public List<String> userl = new ArrayList<>();
    public List<String> passl = new ArrayList<>();
    public List<String> emaill = new ArrayList<>();
    public EditText pass;
    public EditText pass2;
    public EditText email;
    public EditText user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button Login_2 = (Button) findViewById(R.id.Login);
        Button Register = (Button) findViewById(R.id.Register);
        Button Home = (Button) findViewById(R.id.Home);

        user = (EditText) findViewById(R.id.Enter_user);
        email = (EditText) findViewById(R.id.Enter_email);
        pass = (EditText) findViewById(R.id.Enter_pass);
        pass2 = (EditText) findViewById(R.id.Enter_pass_again);

        final TextView error = (TextView) findViewById(R.id.textView3);
        final String pass_1 = pass.getText().toString();
        final String pass_2 = pass.getText().toString();
        final String email_a = email.getText().toString();
        final String user_a = user.getText().toString();

        //false accounts

        Login_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenLogin();
            }
        });



        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user.getText().toString() == ""){
                    error.setText("Enter a username");
                }
                else if(email.getText().toString() == ""){
                    error.setText("Enter a email");
                }
                else if(pass.getText().toString() == "" || pass2.getText().toString() == ""){
                    error.setText("Enter a password");
                }
                else if(!(pass_1.equals(pass_2))){
                    System.out.println(pass.getText().toString() + ", " + pass2.getText().toString());
                    error.setText("Passwords do not match.");
                }
                else{
                    error.setTextColor(Color.BLACK);
                    error.setText("Registered");
                    addList(email_a, user_a, pass_1);
                    OpenMain();
                }


            }
        });

        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenHome();
            }
        });
    }

    public void addList( String email, String user, String pass){
        emaill.add(email);
        userl.add(user);
        passl.add(pass);
    }

    public void OpenHome(){
        Intent intent = new Intent(this, SPlash.class);
        startActivity(intent);
    }

    public void OpenLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void OpenMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
