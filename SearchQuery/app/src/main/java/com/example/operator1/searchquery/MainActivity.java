package com.example.operator1.searchquery;

import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.operator1.searchquery.ListVadapter;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import android.util.JsonReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, SearchView.OnQueryTextListener{

    int[] IMAGES = {R.drawable.applw,R.drawable.arms,R.drawable.bananas,R.drawable.boxinggloves,
            R.drawable.orang,R.drawable.orangejuice,R.drawable.ufc};

    int quantity;
    ListVadapter adapter;
    SearchView editsearch;

    ArrayList<Item> item = new ArrayList<Item>();
    ItemList il = new ItemList();
    @RequiresApi(api = VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Item apple = new Item("Apples","food",2.99,75,75, IMAGES[0]);
        Item arms = new Item("ARMS", "electronics",59.99, 64, 37, IMAGES[1]);
        Item banana = new Item("Bananas","food",3.99,76,75, IMAGES[2]);
        Item boxingGloves = new Item("Boxing Gloves","Sports",12.99,34,77, IMAGES[3]);
        Item oranges = new Item("Oranges","food",1.99,74,75, IMAGES[4]);
        Item orangeJuice = new Item("Orange Juice","food",5.99,23,67, IMAGES[5]);
        Item ufc = new Item("UFC Videogame","electronics",52.99,62,37, IMAGES[6]);

        item.add(apple);
        item.add(arms);
        item.add(banana);
        item.add(boxingGloves);
        item.add(oranges);
        item.add(orangeJuice);
        item.add(ufc);

        for(int i = 0; i < item.size(); i++){
            il.addItem(item.get(i),0);
            item.get(i).marshal();
        }

        il.save();



        //JSONArray loader = new JSONArray();

        //il.load(loader);

        ListView listAdapter = findViewById(R.id.List);
        ListView listAdapter2 = findViewById(R.id.myList);
        SearchView searching = findViewById(R.id.search);

        CustomAdapter ca = new CustomAdapter();
        listAdapter2.setAdapter(ca);

        // Pass results to ListViewAdapter Class
        adapter = new ListVadapter(this, il);

        // Binds the Adapter to the ListView
        listAdapter.setAdapter(adapter);


        // Locate the EditText in listview_main.xml
        editsearch = (SearchView) findViewById(R.id.search);
        editsearch.setOnQueryTextListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
         quantity = (int) parent.getItemAtPosition(position);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        quantity = 0;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapter.filter(text);
        return false;
    }


    class CustomAdapter extends BaseAdapter implements OnItemSelectedListener {

        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.customlayout,null);

           // View ListView = findViewById(R.id.listing);

            ImageView iv = convertView.findViewById(R.id.imageView);
            TextView name = convertView.findViewById(R.id.name);
            TextView price = convertView.findViewById(R.id.price);
            ImageButton ib = convertView.findViewById(R.id.imageButton4);
            Spinner sp = convertView.findViewById(R.id.spinner);

            ArrayAdapter<CharSequence> Adapter = ArrayAdapter.createFromResource(getApplicationContext(),R.array.quantity,android.R.layout.simple_spinner_item);
            Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp.setAdapter(Adapter);
            sp.setOnItemSelectedListener(this);

            iv.setImageResource(il.items.get(i).getPic());
            name.setText(il.items.get(i).getName());
            price.setText(il.items.get(i).getPrice());


            return convertView;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            quantity = Integer.parseInt(parent.getItemAtPosition(position).toString());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            quantity = 0;
        }
    }

    //<<<<<<< HEAD
//List class
/*
 * Item -> List(
 * List functions
 * add/subtract
 * total price
 * Quantity ~ edit Qty
 * size_t Qty
 * add (Item item, int Qty)
 * */

//ItemList Class v0.02a
/*
 * Creation of an item in the program
 * Picture adding coming soon
 * Allows, naming of Item, Category, and positioning (Proce of item included in the mix as well
 */

    public class Item {
        String name;
        String category;
        int pic;

        double price;
        int x;
        int y;
        int qty;

        Item(){
            name = "apple";
            category = "food";
            x = 45;
            y = 34;
            qty = 0;
            pic = 0;
            marshal();
        }

        Item(JSONObject obj){
            try {
                name = obj.getString("name");
                category = obj.getString("category");
                x = (int) obj.getLong("x");
                y = (int) obj.getLong("y");
                price = obj.getDouble("price");
                qty = (int) obj.getLong("qty");
                pic = (int) obj.getLong("pic");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Item(Item i){
            name = i.name;
            category = i.category;
            x = i.x;
            y = i.y;
            qty = i.qty;
            pic = i.pic;
            marshal();
        }

        Item(String Name, String Category, double Price, int X, int Y, int Pic){
            name = Name;
            category = Category;
            price = Price;
            x = X;
            y = Y;
            qty = 0;
            pic = Pic;
            marshal();
        }

        void setQty(int amt){
            this.qty = amt;
        }

        public int getPic(){
            return this.pic;
        }

        public String getName(){
            return this.name;
        }

        public String getPrice(){
            return String.valueOf(price);
        }

        JSONObject marshal()
        {
            JSONObject obj =new JSONObject();
            JSONObject listTube = new JSONObject();
            try {
                obj.put("items", listTube);
                listTube.put("name", name);
                listTube.put("category", category);
                listTube.put("price", price);
                listTube.put("x", x);
                listTube.put("y", y);
                listTube.put("qty", qty);
                listTube.put("pic", pic);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return obj;

        }

    }

    //ItemList Class v0.01a
/*
 * Calcuates total cost of the list
 * Allows Item adding for one or more items
 * Also allows Item deletion of one or more items
 *
 * COming SOON
 * Search
 * */
    public static class ItemList{
        ArrayList<Item> items;
        ArrayList<Item> tempSearch;
        ItemList(){
            items = new ArrayList<Item>();
        }

        ItemList(ItemList ite){
            items = new ArrayList<Item>();
            for(int i = 0; i< ite.items.size(); i++){
                items.add(ite.items.get(i));
            }
        }

        void addItem(Item item, int Qty){
            if(items.contains(item)){
                for(int i = 0; i < items.size(); i++){
                    if(items.get(i).name.equals(item)){
                        items.get(i).setQty(Qty);
                    }
                }
            }
            else{
                item.setQty(Qty);
                items.add(item);
            }
        }


        void deleteItem(Item item){
            for(int i = 0; i < items.size(); i++){
                if(items.get(i).name.equals(item.name) && items.get(i).qty > 1){
                    items.remove(i);
                }
            }
        }

	/*
	 * use contains
	 * update list
	 * --Implement KeyListeners
	 * */

        public double totalPrice(){
            double total= 0;
            for(int i = 0; i< items.size(); i++){
                double itemprice = items.get(i).price * items.get(i).qty;
                total += itemprice;
            }
            return total;
        }

		JSONArray marshal()
        {

            JSONArray listTube = new JSONArray();
            for(int i=0; i< items.size(); i++){
                listTube.put(items.get(i).marshal());
            }
            return listTube;

        }

        void save(){
            JSONArray saver = marshal();
            //saver.save("items.json");
            FileOutputStream fos ;

            try{
                fos = new FileOutputStream(Environment.getExternalStorageDirectory() + "/List.json", true);
                try {
                    FileWriter file = new FileWriter(fos.getFD());
                    file.write(saver.toString());
                    file.flush();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
                finally {
                    fos.getFD().sync();
                    fos.close();
                }
            }
            catch(IOException e){
                e.printStackTrace();
            }
            System.out.println("Saved");
        }

        /*void load(JSONArray arr){
            items= new ArrayList<Item>();
            JSONArray jList = arr.get("items");
            for(int i = 0; i<jList.length(); i++){
                items.add(new Item(jList.get(i)));
            }
            System.out.println("Loaded");
            System.out.println("items");

            String in = "List.json";
            JSONObject reader;
            try {
                reader = new JSONObject(in);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
    }
}
