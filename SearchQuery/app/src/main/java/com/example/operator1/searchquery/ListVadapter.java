package com.example.operator1.searchquery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.operator1.searchquery.MainActivity.Item;
import com.example.operator1.searchquery.MainActivity.ItemList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListVadapter extends BaseAdapter implements OnItemSelectedListener, OnQueryTextListener {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ItemList it = null;
    private ItemList arraylist;
    public ItemList mine;

    public ListVadapter(Context context, ItemList list) {
        mContext = context;
        it = new ItemList(list);
        mine = new ItemList();
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ItemList();
        this.arraylist.items.addAll(it.items);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    public class ViewHolder {
        TextView name;
        ImageView iv ;
        TextView price;
        ImageButton ib ;
        Spinner sp ;
    }
    @Override
    public int getCount() {
        return it.items.size();
    }

    @Override
    public Item getItem(int position) {
        return it.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.customlayout, null);
            // Locate the TextViews in listview_item.xml
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.iv = view.findViewById(R.id.imageView);
            holder.price = view.findViewById(R.id.price);
            holder.ib = view.findViewById(R.id.imageButton4);
            holder.sp = view.findViewById(R.id.spinner);

            ArrayAdapter<CharSequence> Adapter = ArrayAdapter.createFromResource(mContext,R.array.quantity,android.R.layout.simple_spinner_item);
            Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.sp.setAdapter(Adapter);
            holder.sp.setOnItemSelectedListener(this);


            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(it.items.get(position).getName());
        holder.iv.setImageResource(it.items.get(position).getPic());
        holder.price.setText(it.items.get(position).getPrice());
        holder.ib.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(holder.sp.getSelectedItem().toString());
                mine.addItem(it.items.get(position),qty);
            }
        });

        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        it.items.clear();
        if (charText.length() == 0) {
            it.items.addAll(arraylist.items);
        } else {
            for (int i = 0; i< arraylist.items.size(); i++) {
                if (arraylist.items.get(i).getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    it.items.add(arraylist.items.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

}