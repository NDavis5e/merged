package com.example.operator1.home;

import android.content.ClipData.Item;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.operator1.home.Model;

/**
 * Created by lindseyalbin on 3/27/18.
 */

public class LineView extends View
{
    private Paint paint = new Paint();
    Model model;
    public LineView(Context context)
    {
        super(context);
    }

    public LineView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    public LineView(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    void setModel(Model m)
    {
        model = m;
    }
    
    @Override
    protected void onDraw(Canvas canvas)
    {
        Item currentItem = model.items.get(model.index);
        super.onDraw(canvas);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(2);
        canvas.drawLine(currentItem.x, currentItem.y, model.robot.x, model.robot.y, paint);
        model.update();

        invalidate();
        requestLayout();
    }
}