package com.example.operator1.home;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Soo Hyun Jeon on 2018-03-04.
 */

public class ImageAdapter extends ArrayAdapter<String>{
    private Integer[] img;
    private ArrayList<String> items;
    private Activity context;
    ImageAdapter(Activity context, ArrayList<String> items, Integer[] img){
        super(context, R.layout.image_layout, items);

        this.context = context;
        this.items = items;
        this.img = img;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder viewHolder = null;
        if(r == null)
        {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.image_layout, null, true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)r.getTag();
        }
        viewHolder.ivw.setImageResource(img[position]);
        viewHolder.tvw.setText(items.get(position));
        return r;
    }

    class ViewHolder
    {
        TextView tvw;
        ImageView ivw;
        ViewHolder(View v)
        {
            tvw = (TextView) v.findViewById(R.id.textView);
            ivw = (ImageView) v.findViewById(R.id.imageView4);
        }
    }

}