package com.example.operator1.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    Integer[] img = {R.drawable.chocolate, R.drawable.juice, R.drawable.cereal};
    private Button positionButton;
    private Button goButton;
    private Button backButton;
    int index = 0;
    Model model = new Model();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        model.addItem("Chocolate", "Candy", 3,230, 250);
        model.addItem("Orange juice", "Drinks", 3, 570,225);
        model.addItem("Honey Nut Cheerios", "Cereal", 3,500, 420);

        positionButton = findViewById(R.id.positionButton);
        goButton = findViewById(R.id.goButton);
        goButton.setOnClickListener(MainActivity.this);
        backButton = findViewById(R.id.backButton);
        
        ListView listView = findViewById(R.id.listView);
        ImageAdapter adapter = new ImageAdapter(this, model.itemNames, img);
        listView.setAdapter(adapter);

        final LineView lineView = findViewById(R.id.lineView);
        lineView.setModel(model);

        final LocationView locationView = findViewById(R.id.locationView);
        locationView.setModel(model);

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView parent, View view, int position, long id) {
                        switch (position){
                            case 0:
                                model.setIndex(0);
                                break;
                            case 1:
                                model.setIndex(1);
                                break;
                            case 2:
                                model.setIndex(2);
                                break;
                        }
                    }
                }
        );
    }
    
    public void onClick(View v)
    {
        model.setIndex(index);
        index++;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}