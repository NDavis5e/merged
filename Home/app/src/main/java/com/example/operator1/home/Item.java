import java.util.ArrayList;

public class Item {
    String name;
    String category;

    float price;
    float x;
    float y;

    Item(String Name, String Category, float Price, float X, float Y){
        name = Name;
        category = Category;
        price = Price;
        x = X;
        y = Y;
    }

}