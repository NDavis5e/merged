package com.example.operator1.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.operator1.home.Model;
import com.example.operator1.home.R;

/**
 * Created by lindseyalbin on 4/12/18.
 */

public class LocationView extends View {
    Model model;
    Bitmap marker = BitmapFactory.decodeResource(this.getResources(), R.mipmap.marker);


    private Paint paint = new Paint();

    public LocationView(Context context)
    {
        super(context);
    }

    public LocationView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    public LocationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    void setModel(Model m)
    {
        model = m;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        //Item currentItem = model.items.get(model.index);
        for(int i = 0; i < model.items.size(); i++){
            canvas.drawBitmap(marker, model.items.get(i).x, model.items.get(i).y, null);
        }
        super.onDraw(canvas);
        //canvas.drawBitmap(marker, currentItem.x - 23, currentItem.y - 44, null);

        invalidate();
        requestLayout();
    }
}