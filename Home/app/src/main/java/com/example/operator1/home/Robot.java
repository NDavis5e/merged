package com.example.operator1.home;

import android.content.ClipData.Item;
import android.graphics.PointF;

/**
 * Created by lindseyalbin on 3/27/18.
 */

public class Robot {
    PointF home = new PointF(400, 400);
    float x;
    float y;
    
    Robot(float xx, float yy)
    {
        x = xx;
        y = yy;
    }
    
    void move(Item item)
    {
        if (item.x > x)
            x++;
        else
            x--;
        if (item.y < y)
            y--;
        else
            y++;
    }
}