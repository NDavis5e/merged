package com.example.operator1.home;

import android.content.ClipData.Item;

import java.util.ArrayList;

/**
 * Created by lindseyalbin on 4/3/18.
 */

public class Model {
    Robot robot;
    ArrayList<String> itemNames;
    ArrayList<Item> items;
    int index;

    Model()
    {
        robot = new Robot(230, 400);
        itemNames = new ArrayList<>();
        items = new ArrayList<>();
    }

    void addItem(String name, String category, float price, float x, float y)
    {
        Item tmp = new Item(name, category, price, x, y);
        items.add(tmp);
        itemNames.add(name);
    }

    void setIndex(int i)
    {
        index = i;
    }


    void update()
    {
        robot.move(items.get(index));
    }
}